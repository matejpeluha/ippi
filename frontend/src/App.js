import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import './App.css';


import HomePage from "./components/pages/HomePage";
import StatsPage from "./components/pages/StatsPage";
import WeatherPage from "./components/pages/WeatherPage";
import IpAddressPage from "./components/pages/IpAddressPage";




class App extends Component{
  render() {
    return (

        <Router basename={"/ippi"}>
            <Switch>
                <Route path={"/"} exact component={HomePage}/>
                <Route path={"/stats"} exact component={StatsPage}/>
                <Route path={"/weather"} exact component={WeatherPage}/>
                <Route path={"/ip"} exact component={IpAddressPage}/>
            </Switch>
        </Router>

    );
  }
}

export default App;

