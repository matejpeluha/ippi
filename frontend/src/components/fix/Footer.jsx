import React, {Component} from "react"

import "../../App.css";
import "../../css-components/fix/footer.css"

class Footer extends Component {
    render() {
        return (
            <footer>
                <p>
                    © Matej Peluha
                </p>
            </footer>
        );
    }
}

export default Footer;