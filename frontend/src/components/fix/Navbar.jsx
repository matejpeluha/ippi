import React, {Component} from "react"

import "../../App.css";
import "../../css-components/fix/navbar.css"

class Navbar extends Component {
    render() {
        return (
            <nav>
                <a id={"home-icon"} href={"/ippi/"}>
                    <i className="fas fa-home"/>
                </a>
                <div id={"links-container"}>
                    <a href={"/ippi/weather"}>POČASIE</a>
                    <a href={"/ippi/ip"}>IP</a>
                    <a href={"/ippi/stats"}>ŠTATISTIKY</a>
                </div>
            </nav>
        );
    }
}

export default Navbar;