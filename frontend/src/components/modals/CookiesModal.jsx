import React, {Component} from "react"

import "../../css-components/modals/modal.css"



class CookiesModal extends Component {

    state = {
        isOpened: false
    }

    componentDidMount() {
        if(this.getCookie("ip-access") === ""){
            this.setState({isOpened: true});
        }
        else {
            this.props.onCookieAnswer(this.props.pageType);
        }
    }

    render() {

        return (
            <div id={"modal"} className={"w3-container"}>
                <div id="modal-info" className={"w3-modal " + this.getShowModalClass()}>
                    <div id={"modal-content"} className="w3-modal-content w3-animate-top w3-card-4">
                        {this.getModalHeader()}
                        {this.getModalBody()}
                        <footer className="w3-container teal-color" />
                    </div>
                </div>
            </div>

        );
    }


    setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        const expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    getCookie(cname) {
        const name = cname + "=";
        const decodedCookie = decodeURIComponent(document.cookie);
        const ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }


    getShowModalClass(){
        if(this.state.isOpened) {
            return "opened-modal";
        }
        else {
            return "closed-modal";
        }
    }



    getModalHeader(){
        return(
            <header className="w3-container teal-color">
                <h1>Prístup k IP adrese</h1>
            </header>
        );
    }

    getModalBody(){
        return(
            <div className="w3-container modal-body">
                <lottie-player id={"cookies-lottie"} src="https://assets5.lottiefiles.com/packages/lf20_dyq0qz89/data.json"  speed="1"  loop autoplay/>
                <p>
                    Povoľujete web stránke spracovať a ukladať vašu ip adresu ?
                </p>
                <p>
                    Pokiaľ nepríjmete, nebudete môcť stránku používať.
                </p>
                <button id={"accept"} onClick={this.acceptCookies}>Prijať</button>
            </div>
        );
    }

    acceptCookies = () =>{
        this.setCookie("ip-access", "yes", 365)
        this.props.onCookieAnswer(this.props.pageType);
        this.closeModal();
    }


    closeModal(){
        this.setState({isOpened: false});
    }

}


export default CookiesModal;