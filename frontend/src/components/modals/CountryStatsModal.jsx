import React, {Component} from "react"

import "../../css-components/modals/modal.css"



class CountryStatsModal extends Component {


    render() {
        return (
            <div id={"country-stats-modal"} className={"w3-container"}>
                <div id="modal-info" className={"w3-modal modal-info " + this.getShowModalClass()}>
                    <div id={"modal-content"} className="w3-modal-content w3-animate-top w3-card-4">
                        {this.getModalHeader()}
                        {this.getModalBody()}
                        <footer className="w3-container teal-color" />
                    </div>
                </div>
            </div>

        );
    }


    getShowModalClass(){
        if(this.props.info.isOpened) {
            return "opened-modal";
        }
        else {
            return "closed-modal";
        }
    }



    getModalHeader(){
        const iconSource = 'http://www.geonames.org/flags/x/' + this.props.info.data.country_code + '.gif';
        return(
            <header className="w3-container teal-color">
                <span onClick={this.props.onCloseModal} className="w3-button w3-display-topright">&times;</span>
                <h1>{this.props.info.data.country}</h1>
                <img id="modal-image" src={iconSource} alt={"flag"}/>
            </header>
        );
    }

    getModalBody(){
        return(
            <div id={"cities-table-container-all"} className="w3-container modal-body">
                <table id={"cities-table-header"}>
                    <thead>
                    <tr>
                        <th colSpan={2}>
                            Návštevnosť v mestách
                        </th>
                    </tr>
                    </thead>
                </table>
                <div id={"cities-table-container"}>
                    <table id={"cities-table"}>
                        <tbody>
                        {this.props.info.data.allCities.map(this.getRow)}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    getRow = (city, key) =>{
        return <tr key={key}><td>{city.city}</td><td>{city.visits}</td></tr>
    }

}

export default CountryStatsModal;