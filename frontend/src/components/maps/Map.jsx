import React, {Component} from 'react';
import L from "leaflet"

import "../../css-components/maps/map.css"

class Map extends Component {

    render() {
        return (
            <div id="map" className={"transparent-border"}/>
        );
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        let map = L.map('map').setView([0, 0], 1.2);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWF0ZWpwZWx1aGEiLCJhIjoiY2tvMWVoNWgxMDd0NTJ4bHB3MGR0bWYzeCJ9.I0fQgeY7ieY-OYOn1FB1gw', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);


        for (let gps of this.props.gps) {
            L.marker([gps.latitude, gps.longitude]).addTo(map)
                .bindPopup(gps.city);
        }
    }


}


export default Map;