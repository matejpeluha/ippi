import React, {Component} from 'react'

import "../../css-components/tables/table.css"
import "../../css-components/tables/page-table.css"
import  "../../App.css"

class PageTable extends Component {
    render() {
        return (
            <table id={"page-table"}>
                <thead>
                    <tr>
                        <th colSpan={2}>
                            Návštevnosť stránok
                        </th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Domov</td><td>{this.props.stats.home_count}</td>
                </tr>
                <tr>
                    <td>Ip adresa</td><td>{this.props.stats.ip_count}</td>
                </tr>
                <tr>
                    <td>Štatistiky</td><td>{this.props.stats.stats_count}</td>
                </tr>
                <tr>
                    <td>Počasie</td><td>{this.props.stats.weather_count}</td>
                </tr>
                </tbody>
            </table>
        );
    }
}


export default PageTable;