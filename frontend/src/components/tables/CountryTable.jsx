import React, {Component} from 'react'

import "../../css-components/tables/table.css"
import "../../css-components/tables/country-table.css"
import  "../../App.css"
import CountryStatsModal from "../modals/CountryStatsModal";

class CountryTable extends Component {
    state = {
        modalInfo: {
            isOpened: false,
            data: {
                country: "",
                country_code: "sk",
                visits: "",
                allCities: []
            }
        }
    }


    render() {
        return (
            <div id={"country-table-all-container"}>
                <CountryStatsModal info={this.state.modalInfo} onCloseModal={this.closeModal}/>
                <table id={"sticky-table"}>
                    <thead>
                    <tr>
                        <th colSpan={2}>
                            Návštevnosť zo sveta
                        </th>
                    </tr>
                    </thead>
                </table>
                <div id={"country-table-container"}>
                    <table id={"country-table"}>
                        <tbody>
                        {this.props.stats.map(this.getRow)}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }



    getRow = (visit, key) =>{
        const iconSource = 'http://www.geonames.org/flags/x/' + visit.country_code + '.gif';
        return <tr onClick={() => this.openModal(visit)} key={key}><td><img id={"flag"} src={iconSource} alt={"flag"} /> {visit.country}</td><td>{visit.visits}</td></tr>
    }

    openModal(country){
        const newState = {
            modalInfo: {
                isOpened: true,
                data: country
            }
        }
        this.setState(newState);
    }

    closeModal = () =>{
        const newState = {
            modalInfo: {
                isOpened: false,
                data: {
                    country: "",
                    country_code: "sk",
                    visits: "",
                    allCities: []
                }
            }
        }
        this.setState(newState);
    }
}


export default CountryTable;