import React, {Component} from 'react'

import "../../css-components/tables/table.css"
import "../../css-components/tables/time-table.css"
import  "../../App.css"

class TimeTable extends Component {
    render() {
        return (
            <table id={"time-table"}>
                <thead>
                <tr>
                    <th colSpan={2}>
                        Denná návštevnosť
                    </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>24:00-06:00</td><td>{this.props.stats.zero_six_count}</td>
                    </tr>
                <tr>
                    <td>06:00-15:00</td><td>{this.props.stats.six_fifteen_count}</td>
                </tr>
                <tr>
                    <td>15:00-21:00</td><td>{this.props.stats.fifteen_twentyone_count}</td>
                </tr>
                <tr>
                    <td>21:00-24:00</td><td>{this.props.stats.twentyone_zero_count}</td>
                </tr>
                </tbody>
            </table>
        );
    }
}


export default TimeTable;