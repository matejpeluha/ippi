import {Component} from 'react'

class Page extends Component{

    saveIp(){
        console.log("pass");
    }


    postSingleVisits(pageName){
        const date = new Date();
        const hour = date.getHours();
        const inputObject = {pageName: pageName, hour: hour};
        const jsonRepresentation = JSON.stringify(inputObject);

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');


        let initObject = {
            method: 'POST', headers: reqHeader, body: jsonRepresentation
        };

        const userRequest = new Request('https://wt118.fei.stuba.sk/ippi/api/single_visits/', initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.changeResult)
            .catch(this.printError);
    }


    getJsonFromResponse = (response) =>{
        return response.json();
    }


    changeResult = (data) =>{
       // console.log(data);
    }


    printError = (err) =>{
        console.log("Something went wrong!", err);
    }


    postDailyVisit = (json) =>{
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'POST', headers: reqHeader, body: JSON.stringify(json)
        };


        const userRequest = new Request('https://wt118.fei.stuba.sk/ippi/api/ip_visits/', initObject);

        fetch(userRequest).catch(this.printError);
    }

    getToday(){
        const date = new Date();
        const month = date.getMonth() + 1;
        const day = date.getDate();
        return day + "." + month + ".";
    }

    loadInitState = (pageType) =>{
        this.postSingleVisits(pageType);

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const userRequest = new Request('https://api.ip2loc.com/MLHyTwC0hgjWPFboxxcf4rcCIc4A4Pj0/detect', initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.saveInitResult)
            .catch(this.printError);
    }

    saveInitResult = (data) =>{
        let city = data.location.city;
        if(!city){
            city = "Nemožno nájsť";
        }

        const date = this.getToday();

        const country_code = data.location.country.alpha_2.toLowerCase();
        const dailyVisitState = {
            ip: data.connection.ip,
            date: date,
            gpsLatitude: data.location.latitude,
            gpsLongitude: data.location.longitude,
            city: city,
            country_code: country_code,
            country: data.location.country.name
        }
        this.postDailyVisit(dailyVisitState);
    }

}

export default Page;