import React from "react"

import "../../App.css";
import "../../css-components/pages/home.css"
import Footer from "../fix/Footer";
import CookiesModal from "../modals/CookiesModal";
import Page from "../abstract/Page";


class HomePage extends Page {
    render() {
        return (
            <React.Fragment>
                <CookiesModal pageType={"home"} onCookieAnswer={this.loadInitState}/>
                <main id={"home-page"} className={"page-container"}>
                    <div id={"home-buttons-container"}>
                        <a href={"/ippi/weather"} >
                            <button className={"box-button"}>Počasie</button>
                        </a>
                        <a href={"/ippi/ip"}>
                            <button className={"box-button"}>IP Adresa</button>
                        </a>
                        <a href={"/ippi/stats"}>
                            <button className={"box-button"}>Štatistiky</button>
                        </a>
                    </div>
                    <Footer/>
                </main>

            </React.Fragment>
        );
    }

}

export default HomePage;