import React from "react"

import "../../App.css";
import "../../css-components/pages/ip-address.css";
import Navbar from "../fix/Navbar";
import Footer from "../fix/Footer";
import CookiesModal from "../modals/CookiesModal";
import Page from "../abstract/Page";

class IpAddressPage extends Page {
    state ={
        ip: "",
        date: "",
        gpsLatitude: "",
        gpsLongitude: "",
        city: "",
        country: "",
        countryCapital: ""
    }

    render() {
        return (
            <React.Fragment>
                <CookiesModal pageType={"ip"} onCookieAnswer={this.loadInitState}/>
                <Navbar/>
                <div id={"ip-address-page"} className={"page-container"}>
                    <div id={"ip-address"}><span>IP adresa: </span><h2>{this.state.ip}</h2></div>
                    <div id={"ip-info"}>
                        <div id={"gps"}>
                            <p>Súradnice</p>
                            <p>x: {this.state.gpsLatitude}</p>
                            <p>y: {this.state.gpsLongitude}</p>
                        </div>
                        <div id={"location"}>
                            <p>mesto: {this.state.city}</p>
                            <p>krajina: {this.state.country}</p>
                            <p>[hl.mesto: {this.state.countryCapital}]</p>
                        </div>
                    </div>
                    <Footer/>
                </div>

            </React.Fragment>
        );
    }


    saveInitResult = (data) =>{
        let city = data.location.city;
        if(!city){
            city = "Nemožno nájsť";
        }

        const date = this.getToday();

        const country_code = data.location.country.alpha_2.toLowerCase();
        const newState = {
            ip: data.connection.ip,
            date: date,
            gpsLatitude: data.location.latitude,
            gpsLongitude: data.location.longitude,
            city: city,
            country_code: country_code,
            country: data.location.country.name,
            countryCapital: data.location.capital
        };
        this.setState(newState, this.postStateToDailyVisit);
    }

    postStateToDailyVisit = () =>{
        this.postDailyVisit(this.state);
    }


}

export default IpAddressPage;