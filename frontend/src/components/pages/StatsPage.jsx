import React from "react"

import Navbar from "../fix/Navbar";
import Footer from "../fix/Footer";
import CookiesModal from "../modals/CookiesModal";
import Page from "../abstract/Page";
import TimeTable from "../tables/TimeTable";
import PageTable from "../tables/PageTable";
import CountryTable from "../tables/CountryTable";

import "../../css-components/pages/stats.css"
import Map from "../maps/Map";


class StatsPage extends Page {
    state = {
        allGps: [],
        ipVisits: [],
        singleVisits: {
            ip_count: "0",
            weather_count: "0",
            stats_count: "0",
            home_count: "0"
        },
        timeVisits: {
            zero_six_count: "0",
            six_fifteen_count: "0",
            fifteen_twentyone_count: "0",
            twentyone_zero_count: "0"
        }
    }



    render() {
        return (
            <React.Fragment>
                <CookiesModal pageType={"stats"} onCookieAnswer={this.loadInitState}/>
                <Navbar/>
                <div id={"stats-page"} className={"page-container"}>
                    <div>
                        <div id={"tables-container"}>
                            <TimeTable stats={this.state.timeVisits}/>
                            <CountryTable stats={this.state.ipVisits}/>
                            <PageTable stats={this.state.singleVisits}/>
                        </div>
                    </div>
                    <Map gps={this.state.allGps}/>
                    <Footer/>
                </div>
            </React.Fragment>
        );
    }


    saveInitResult = (data) =>{
        let city = data.location.city;
        if(!city){
            city = "Nemožno nájsť";
        }

        const date = this.getToday();

        const country_code = data.location.country.alpha_2.toLowerCase();

        document.getElementById("map").classList.remove("transparent-border")
        const dailyVisitState = {
            ip: data.connection.ip,
            date: date,
            gpsLatitude: data.location.latitude,
            gpsLongitude: data.location.longitude,
            city: city,
            country_code: country_code,
            country: data.location.country.name
        }
        this.postDailyVisit(dailyVisitState);
    }

    postDailyVisit = (json) =>{
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'POST', headers: reqHeader, body: JSON.stringify(json)
        };


        const userRequest = new Request('https://wt118.fei.stuba.sk/ippi/api/ip_visits/', initObject);

        fetch(userRequest)
            .then(this.loadStats)
            .catch(this.printError);
    }

    loadStats = () =>{
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const userRequest = new Request('https://wt118.fei.stuba.sk/ippi/api/', initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.handleJson)
            .catch(this.printError);
    }

    handleJson = (json) =>{
        this.setState(json, () => console.log(this.state))
    }


}

export default StatsPage;