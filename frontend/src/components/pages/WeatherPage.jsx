import React from "react"
import Navbar from "../fix/Navbar";
import Footer from "../fix/Footer";

import "../../App.css"
import "../../index.css"
import "../../css-components/pages/weather.css"
import CookiesModal from "../modals/CookiesModal";
import Page from "../abstract/Page";



class WeatherPage extends Page {

    state = {
        lottie: "",
        allGps: {
            "Cairo": {
                gpsLat: "30.033333",
                gpsLong: "31.233334",
                name: "Káhira",
            },
            "NewYork": {
                gpsLat: "40.730610",
                gpsLong: "-73.935242",
                name: "New York",
            },
            "Moscow": {
                gpsLat: "55.751244",
                gpsLong: "37.618423",
                name: "Moskva",
            }
            ,
            "Longyearbyen": {
                gpsLat: "78.2166658",
                gpsLong: "15.5499978",
                name: "Longyearbyen",
            },
            "NewDelhi": {
                gpsLat: "28.644800",
                gpsLong: "77.216721",
                name: "New Delhi",
            },
            "Hanoi": {
                gpsLat: "21.028511",
                gpsLong: "105.804817",
                name: "Hanoi",
            }
        },
        city: {
            gpsLat: "",
            gpsLong: "",
            name: "me"
        },
        weather: {
            condition: "",
            conditionText: "",
            temperature: "",
            humidity: "",
            wind: "",
            clouds: "",
            pressure: ""
        }
    }


    constructor(props) {
        super(props);
        document.getElementById("background-image").removeAttribute("class");
        document.getElementById("background-image").classList.add("blank");
    }


    render() {
        return (
            <React.Fragment>
                <CookiesModal pageType={"weather"} onCookieAnswer={this.loadInitState}/>
                <Navbar/>
                <div id={"weather-page"} className={"page-container"}>
                    <div id={"weather-info"}>
                        <h2>{this.state.city.name}</h2>
                        <select onChange={this.changeWeather}>
                            <option value={"me"}>Moja poloha</option>
                            <option value={"Cairo"}>Káhira</option>
                            <option value={"NewYork"}>New York</option>
                            <option value={"Moscow"}>Moskva</option>
                            <option value={"Longyearbyen"}>Longyearbyen</option>
                            <option value={"NewDelhi"}>New Delhi</option>
                            <option value={"Hanoi"}>Hanoi</option>
                        </select>
                        <div id={"lottie-container"}>
                            {this.getLottiePlayer()}
                        </div>
                        <p>{this.state.weather.conditionText}</p>
                        <h1>{this.getTemperatureText()}</h1>

                        <table id={"weather-detail"}>
                            <tbody>
                            <tr>
                                <td><i className="fas fa-wind"/></td>
                                <td>{this.state.weather.wind} m/h</td>
                            </tr>
                            <tr>
                                <td><i className="fas fa-tint"/></td>
                                <td>{this.state.weather.humidity} %</td>
                            </tr>
                            <tr>
                                <td><i className="fas fa-cloud"/></td>
                                <td>{this.state.weather.clouds} %</td>
                            </tr>
                            <tr>
                                <td><i className="fas fa-sort-amount-down-alt"/></td>
                                <td>{this.state.weather.pressure} MB</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <Footer/>
                </div>
            </React.Fragment>
        );
    }

    getLottiePlayer(){
            //return <lottie-player src="https://assets8.lottiefiles.com/temp/lf20_rpC1Rd.json" id={"lottie"} speed="1" loop autoplay/>


        const condition = this.state.weather.condition;
        if(condition === 1063 || condition === 1069 || condition === 1072 || condition === 1087 ||  (condition >= 1150 && condition <= 1207) || (condition >= 1240 && condition <= 1252) || (condition >= 1273 && condition <= 1276)){
            return <lottie-player src="https://assets8.lottiefiles.com/temp/lf20_XkF78Y.json" id={"lottie"} speed="1" loop autoplay/>
        }
        else if(condition === 1066 || condition === 1114 || condition === 1117 || (condition >= 1210 && condition <= 1237) || (condition >= 1252 && condition <= 1264) || (condition >= 1279 && condition <= 1282)){
            return <lottie-player src="https://assets8.lottiefiles.com/temp/lf20_WtPCZs.json" id={"lottie"} speed="1" loop autoplay/>
        }
        else if(condition === 1000){
            return <lottie-player src="https://assets3.lottiefiles.com/temp/lf20_Stdaec.json" id={"lottie"} speed="1" loop autoplay/>
        }
        else if(condition >= 1003 && condition <= 1009){
            return <lottie-player src="https://assets8.lottiefiles.com/temp/lf20_dgjK9i.json" id={"lottie"} speed="1" loop autoplay/>
        }
        else if(condition === 1030 ||condition === 1135||condition === 1147){
            return <lottie-player src="https://assets8.lottiefiles.com/temp/lf20_kOfPKE.json" id={"lottie"} speed="1" loop autoplay/>
        }
    }

    changeWeather = (event) =>{
        const country = event.currentTarget.value;
        if(country === "me"){
            this.loadState();
        }
        else {
            const newState = {
                allGps: this.state.allGps,
                lottie: this.state.lottie,
                city: this.state.allGps[country],
                weather: {}
            }

            this.setState(newState, this.loadWeather);
        }
    }



    saveInitResult = (data) =>{
        let city = data.location.city;
        if(!city){
            city = "Nemožno nájsť";
        }

        const date = this.getToday();

        const country_code = data.location.country.alpha_2.toLowerCase();
        const dailyVisitState = {
            ip: data.connection.ip,
            date: date,
            gpsLatitude: data.location.latitude,
            gpsLongitude: data.location.longitude,
            city: city,
            country_code: country_code,
            country: data.location.country.name
        }
        this.postDailyVisit(dailyVisitState);

        this.saveResult(data);
    }

    loadState = () =>{
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const userRequest = new Request('https://api.ip2loc.com/MLHyTwC0hgjWPFboxxcf4rcCIc4A4Pj0/detect', initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.saveResult)
            .catch(this.printError);

    }

    saveResult = (data) =>{
        const newState = {
            allGps: this.state.allGps,
            lottie: this.state.lottie,
            city: {
                name: data.location.city,
                gpsLat: data.location.latitude,
                gpsLong: data.location.longitude
            },
            weather: this.state.weather
        }
        this.setState(newState, this.loadWeather);
    }



    loadWeather = () =>{
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');
        reqHeader.append('Access-Control-Allow-Origin', '*');
        reqHeader.append("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
        reqHeader.append("Access-Control-Allow-Headers", "*")


        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const url = 'https://api.weatherapi.com/v1/current.json?key=c795184eed094dfa8ae84909212604&q='
                        + this.state.city.gpsLat + ',' + this.state.city.gpsLong +
                        '&aqi=no&lang=sk'
        const userRequest = new Request(url, initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.readResult)
            .catch(this.printError);
    }


    readResult = (data) =>{
        const newState = {
            allGps: this.state.allGps,
            lottie: this.state.lottie,
            city: this.state.city,
            weather: {
                condition: data.current.condition.code,
                conditionText: data.current.condition.text,
                temperature: data.current.temp_c,
                humidity: data.current.humidity,
                wind: data.current.wind_mph,
                clouds: data.current.cloud,
                pressure: data.current.pressure_mb
            }
        }

        this.setState(newState, this.setBackground);
    }

    getJsonFromResponse = (response) =>{
        return response.json();
    }


    printError = (err) =>{
        console.log("Something went wrong!", err);
    }


    setBackground = () =>{
        document.getElementById("background-image").removeAttribute("class");

        const backgroundClass = this.getBackgroundClass();
        document.getElementById("background-image").classList.add(backgroundClass);
    }

    getBackgroundClass(){
        const city = this.state.city.name;
        if(city === "Káhira"){
            return "cairo-image"
        }
        else if(city === "New York"){
            return "ny-image";
        }
        else if(city === "Moskva"){
            return "moscow-image";
        }
        else if(city === "Hanoi"){
            return "hanoi-image"
        }
        else if(city === "New Delhi"){
            return "delhi-image";
        }
        else if(city === "Longyearbyen"){
            return "svalbard-image";
        }
        else {
            return "me-image";
        }
    }

    getTemperatureText(){
        return  Math.floor(this.state.weather.temperature) + "°C";
    }
}

export default WeatherPage;
