<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


const FLAGS = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
require_once("../DatabaseHandler.php");

if($_SERVER["REQUEST_METHOD"] === 'GET'){
    handleGetRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'POST'){
    handlePostRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'OPTIONS'){
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
    header("HTTP/1.1 200 OK");
    die();
}
else{
    http_response_code(405);
}


/*
 * GET
 */

function handleGetRequest(){

}



/*
 * POST
 */

function handlePostRequest(){
    $data = getInputJsonData();

    $ip = $data->ip;
    $date = $data->date;
    $city = $data->city;
    $country = $data->country;
    $countryCode = $data->country_code;
    $latitude = $data->gpsLatitude;
    $longitude = $data->gpsLongitude;

    $query = "INSERT INTO visits (ip, date, city, country, country_code, latitude, longitude) VALUES (?,?,?,?,?,?,?)";
    $bindParameters = [$ip, $date, $city, $country, $countryCode, $latitude, $longitude];

    $databaseHandler = new DatabaseHandler();
    $databaseHandler->pushToDatabase($query, $bindParameters);

    echo json_encode(["ok" => true], FLAGS);
}

function getInputJsonData(){
    $json = file_get_contents('php://input');
    return json_decode($json, false);
}