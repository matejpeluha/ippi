<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

const FLAGS = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
require_once("../DatabaseHandler.php");

if($_SERVER["REQUEST_METHOD"] === 'GET'){
    handleGetRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'POST'){
    handlePostRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'OPTIONS'){
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
    header("HTTP/1.1 200 OK");
    die();
}
else{
    http_response_code(405);
}


/*
 * GET
 */

function handleGetRequest(){
    $query = "SELECT 
                COUNT(case when hour>=0 and hour<6 then 1 else null end) as zero_six_count,
                COUNT(case when hour>=6 and hour<15 then 1 else null end) as six_fifteen_count,
                COUNT(case when hour>=15 and hour<21 then 1 else null end) as fifteen_twentyone_count,
                COUNT(case when hour>=21 and hour<=23 then 1 else null end) as twentyone_zero_count
                FROM `single_visits`";
    $databaseHandler = new DatabaseHandler();
    $response = $databaseHandler->getFromDatabase($query, []);
    echo json_encode($response, FLAGS);
}


