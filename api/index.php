<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

const FLAGS = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;

require_once("DatabaseHandler.php");

if($_SERVER["REQUEST_METHOD"] === 'GET'){
    handleGetRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'POST'){
    handlePostRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'OPTIONS'){
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
    header("HTTP/1.1 200 OK");
    die();
}
else{
    http_response_code(405);
}


/*
 * GET
 */

function handleGetRequest(){
    $singleVisits = getSingleVisits();
    $timeVisits = getTimeVisits();
    $ipVisits = getIpVisits();
    $allGps = getAllGps();

    $response = ["singleVisits" => $singleVisits, "timeVisits" => $timeVisits, "ipVisits" => $ipVisits, "allGps" => $allGps];
    echo json_encode($response, FLAGS);
}


function getAllGps(){
    $query = "SELECT visits.latitude, visits.longitude, visits.city FROM visits";
    $databaseHandler = new DatabaseHandler();
    return $databaseHandler->getArrayFromDatabase($query, []);
}

function getSingleVisits(){
    $query = "SELECT COUNT(case when page = 'ip' then 1 else null end) as ip_count, 
                COUNT(case when page = 'weather' then 1 else null end) as weather_count, 
                COUNT(case when page = 'stats' then 1 else null end) as stats_count,
                COUNT(case when page = 'home' then 1 else null end) as home_count
                FROM `single_visits`";
    $databaseHandler = new DatabaseHandler();
    return $databaseHandler->getFromDatabase($query, []);
}

function getTimeVisits(){
    $query = "SELECT 
                COUNT(case when hour>=0 and hour<6 then 1 else null end) as zero_six_count,
                COUNT(case when hour>=6 and hour<15 then 1 else null end) as six_fifteen_count,
                COUNT(case when hour>=15 and hour<21 then 1 else null end) as fifteen_twentyone_count,
                COUNT(case when hour>=21 and hour<=23 then 1 else null end) as twentyone_zero_count
                FROM `single_visits`";
    $databaseHandler = new DatabaseHandler();
    return $databaseHandler->getFromDatabase($query, []);
}

function getIpVisits(){
    $allCountries = getAllCountries();

    $ipVisits = [];
    foreach ($allCountries as $country){
        $cities = getCityVisits($country);
        $visitsCount = getCountryVisitsCount($country);
        $ipVisits[] = ["country" => $country["country"],"country_code" => $country["country_code"],"visits" => $visitsCount["COUNT(*)"], "allCities" => $cities];
    }
    return $ipVisits;
}

function getAllCountries(){
    $query = "SELECT DISTINCT visits.country, visits.country_code FROM visits";
    $databaseHandler = new DatabaseHandler();
    return $databaseHandler->getArrayFromDatabase($query, []);
}

function getCountryVisitsCount($country){
    $query = "SELECT COUNT(*) FROM visits WHERE visits.country=:country";
    $bindParameters = [":country" => $country["country"]];
    $databaseHandler = new DatabaseHandler();
    return $databaseHandler->getFromDatabase($query, $bindParameters);
}

function getCityVisits($country){
    $query = "SELECT DISTINCT city FROM visits WHERE visits.country=:country";
    $bindParameters = [":country" => $country["country"]];
    $databaseHandler = new DatabaseHandler();
    $allCities = $databaseHandler->getArrayFromDatabase($query, $bindParameters);

    $cityVisits = [];
    foreach ($allCities as $city){
        $visitsCount = getCityVisitsCount($city);
        $cityVisits[] = ["city" => $city["city"], "visits" => $visitsCount["COUNT(*)"]];
    }

    return $cityVisits;
}

function getCityVisitsCount($city){
    $query = "SELECT COUNT(*) FROM visits WHERE visits.city=:city";
    $bindParameters = [":city" => $city["city"]];
    $databaseHandler = new DatabaseHandler();
    return $databaseHandler->getFromDatabase($query, $bindParameters);
}