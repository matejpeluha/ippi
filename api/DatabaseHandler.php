<?php

require_once("config.php");

class DatabaseHandler
{
    private $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    //$query = "SELECT * FROM table WHERE table.id=:id AND table.name=:name"
    //$bindParameters = [":id" => 5, ":name" => "Rumburak"]
    public function getFromDatabase($query, $bindParameters){
        $statement = $this->connection->prepare($query);
        $statement->execute($bindParameters);

        $statement->setFetchMode(PDO::FETCH_ASSOC);
        return $statement->fetchAll()[0];
    }

    public function getArrayFromDatabase($query, $bindParameters){
        $statement = $this->connection->prepare($query);
        $statement->execute($bindParameters);

        $statement->setFetchMode(PDO::FETCH_ASSOC);
        return $statement->fetchAll();
    }

    //$query = "INSERT INTO table (name, work) VALUES (:name, :work)"
    //$bindParameters = [":name" => "Miro Pele", ":work" => "rapper"]
    public function pushToDatabase($query, $bindParameters){
        try {
            $statement = $this->connection->prepare($query);
            $statement->execute($bindParameters);
        }
        catch (Exception $exception){

        }
    }

}