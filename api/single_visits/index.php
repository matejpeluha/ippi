<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

const FLAGS = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
require_once("../DatabaseHandler.php");

if($_SERVER["REQUEST_METHOD"] === 'GET'){
    handleGetRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'POST'){
    handlePostRequest();
}
else if($_SERVER["REQUEST_METHOD"] === 'OPTIONS'){
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
    header("HTTP/1.1 200 OK");
    die();
}
else{
    http_response_code(405);
}


/*
 * GET
 */

function handleGetRequest(){
    $query = "SELECT COUNT(case when page = 'ip' then 1 else null end) as ip_count, 
                COUNT(case when page = 'weather' then 1 else null end) as weather_count, 
                COUNT(case when page = 'stats' then 1 else null end) as stats_count,
                COUNT(case when page = 'home' then 1 else null end) as home_count
                FROM `single_visits`";
    $databaseHandler = new DatabaseHandler();
    $response = $databaseHandler->getFromDatabase($query, []);
    echo json_encode($response, FLAGS);
}



/*
 * POST
 */

function handlePostRequest(){
    $data = getInputJsonData();
    $pageName = $data->pageName;
    $hour = $data->hour;
    $query = "INSERT INTO single_visits (page, hour) VALUES (:page, :hour)";
    $bindParameters = [":page" => $pageName, ":hour" => $hour];
    $databaseHandler = new DatabaseHandler();
    $databaseHandler->pushToDatabase($query, $bindParameters);
    echo json_encode(["ok" => true], FLAGS);
}

function getInputJsonData(){
    $json = file_get_contents('php://input');
    return json_decode($json, false);
}
